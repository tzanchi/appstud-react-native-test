import React from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    ActivityIndicator,
    View,
    Button
} from 'react-native';

import { MonoText } from '../components/StyledText';
import theme from '../constants/Theme';
import styles from '../constants/Styles'
import Playlist from '../components/Playlists';

export default class PlaylistScreen extends React.Component {
    static navigationOptions = {
        title: "Playlists",
        headerStyle: {
            backgroundColor: theme.colors.black,
        },
        headerTintColor: theme.colors.black,
        headerTitleStyle: {
            fontWeight: 'bold',
            color: theme.colors.white
        },
    }

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            playlists: null,
            error: null
        }
    }

    getPlaylists = () => {
        this.setState({ loading: true, playlists: null, error: null })
        fetch("https://afternoon-waters-49321.herokuapp.com/v1/browse/featured-playlists", { method: "GET" })
            .then(res => res.json())
            .then(resJson => {
                this.setState({
                    loading: false,
                    playlists: resJson
                })
            })
            .catch(err => this.setState({ loading: false, error: err }))
    }

    navigateToPlaylist = ({playlist}) => {
        const { navigate } = this.props.navigation || {}
        navigate('Tracks', {playlist: playlist})
    }

    componentDidMount() {
        this.getPlaylists()
    }

    render() {
        const { loading, error, playlists } = this.state || {}

        if (loading) {
            return (
                <LoadingBlock />
            )
        }
        else if (error) {
            return (
                <ErrorBlock getPlaylists={this.getPlaylists} />
            )
        }
        else {
            return (
                <Playlist playlists={playlists} getPlaylistInfo={this.navigateToPlaylist}/>
            )
        }
    }
}

const LoadingBlock = () => {
    return (
        <View style={styles.getStartedContainer}>
            <ActivityIndicator size="large" color={theme.colors.black} />
        </View>
    )
}

const ErrorBlock = (props) => {
    const { getPlaylists } = props || {}

    return (
        <View style={styles.getStartedContainer}>
            <MonoText style={styles.getStartedText}>Sorry an error occured ...</MonoText>
            <Button color={theme.colors.black} title="Get Playlists" onPress={getPlaylists} />
        </View>
    )
}

const SuccessBlock = (props) => {
    const { getPlaylists } = props || {}

    return (
        <View style={styles.getStartedContainer}>
            <MonoText style={styles.getStartedText}>Success</MonoText>
        </View>
    )
}
