import React from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    ActivityIndicator,
    View,
    Button,
    Image,
    ScrollView,
    TouchableOpacity
} from 'react-native';

import { MonoText } from '../components/StyledText';
import theme from '../constants/Theme';
import styles from '../constants/Styles'
import TracksList from '../components/TracksList';

export default class TracksScreen extends React.Component {
    static navigationOptions = {
        headerStyle: {
            backgroundColor: theme.colors.black,
            elevation: 0
        },
    }

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            tracks: null,
            error: null
        }
    }

    getTracks = () => {
        const { playlist } = this.props.navigation.state.params || {}
        const { id } = playlist || {}

        this.setState({ loading: true, playlist: null, error: null })
        fetch("https://afternoon-waters-49321.herokuapp.com/v1/playlists/"+id, { method: "GET" })
            .then(res => res.json())
            .then(resJson => {
                this.setState({
                    loading: false,
                    tracks: resJson
                })
            })
            .catch(err => this.setState({ loading: false, error: err }))
    }

    componentDidMount() {
        this.getTracks()
    }

    render() {
        const { playlist } = this.props.navigation.state.params || {}
        const { loading, tracks, error } = this.state || {}

        return(
            <View style={styles.container}>
                <View style={styles.infoHeader}>
                    <View>
                        <Image source={{uri: playlist.images[0].url}} style={{width: 100, height: 100, marginHorizontal: 15}} />
                    </View>
                    <View style={styles.headerContainer}>
                        <MonoText style={styles.title}>{playlist.name}</MonoText>
                        <MonoText style={styles.subTitle}>Playlist by {playlist.owner.display_name}</MonoText>
                        {
                            (tracks) &&
                            ((tracks.description) && 
                            <View style={styles.container}>
                                <MonoText style={styles.subTitleDescription}>{tracks.description}</MonoText>
                            </View>)
                        }
                    </View>
                </View>
                <View style={styles.container}>
                    {
                        (loading) && <LoadingBlock />
                    }
                    {
                        (error) && <ErrorBlock getTracks={this.getTracks} />
                    }
                    {
                        (tracks) && <TracksList tracks={tracks} />
                    }
                </View>
            </View>
        )
    }
}

const LoadingBlock = () => {
    return (
        <View style={styles.getStartedContainer}>
            <ActivityIndicator size="large" color={theme.colors.black} />
        </View>
    )
}

const ErrorBlock = (props) => {
    const { getTracks } = props || {}

    return (
        <View style={styles.getStartedContainer}>
            <MonoText style={styles.getStartedText}>Sorry an error occured ...</MonoText>
            <Button color={theme.colors.black} title="Get Playlists" onPress={getTracks} />
        </View>
    )
}