import React from 'react';
import {
    ScrollView,
    TouchableOpacity,
    View,
    Alert
} from 'react-native'

import { MonoText } from '../components/StyledText'
import theme from '../constants/Theme'
import styles from '../constants/Styles'

export default class Playlist extends React.Component {
    constructor(props) {
        super(props)
    }

    handlePlay = ({track}) => {
        Alert.alert(
            track.track.name,
            track.track.artists[0].name,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        );
    }

    render() {
        const { tracks } = this.props || {}
        const { items } = tracks.tracks || {}
        
        return (
            <View style={styles.container}>
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    {
                        items.map((item, i) =>
                            <View key={i} style={styles.trackContainer}>
                                <TouchableOpacity
                                    disabled={!item.track.preview_url}
                                    style={{ opacity: (item.track.preview_url) ? 1 : 0.3, width: "100%" }}
                                    onPress={() => this.handlePlay({track: item})}
                                >
                                    <MonoText style={styles.titleBlack}>{item.track.name}</MonoText>
                                    <MonoText style={styles.subTitleBlack}>{item.track.artists[0].name}</MonoText>
                                </TouchableOpacity>
                            </View>
                        )
                    }
                </ScrollView>
            </View>
        )
    }
}