import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    ActivityIndicator,
    TouchableOpacity,
    View,
    Button
} from 'react-native';

import { MonoText } from '../components/StyledText';
import theme from '../constants/Theme';
import styles from '../constants/Styles'

export default class Playlist extends React.Component {
    constructor(props) {
        super(props)
    }

    getPlaylistsInfo = ({ playlist }) => {

    }

    render() {
        const { getPlaylistInfo } = this.props || {}
        const { playlists } = this.props || {}
        const { items } = playlists.playlists || {}

        return(
            <View style={styles.container}>
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    {
                        items.map((item, i) => 
                            <View key={i} style={styles.imageContainer}>
                                <TouchableOpacity onPress={() => getPlaylistInfo({playlist: item})}>
                                    <Image 
                                        source={{uri: item.images[0].url}} 
                                        style={styles.image} 
                                        on
                                    />
                                </TouchableOpacity>
                            </View>
                        )
                    }
                </ScrollView>
            </View>
        )
    }
}
